A Python tool which leverages openCVs homography tools to align two images (a reference and a target) and extract areas of interest.


Originally developed for use with Markbox

### Area of Interest Description

In order to use cut-it-out, you will need to define a set of areas of interest.

This is done via a simple JSON definition of the areas like so:

```json
{
    "label": {"type": "rect", "x": 100, "y": 100, "w": 100, "h": 100},
    "group": {"type": "group", "items": [
        "item1": {"type": "rect"...},
        ...
    ]}
}
```

The specified coordinates should be relative to the reference image

### Commandline Use

Once installed via pip, the ``cutout`` command will be available on the commandline. It can be used to quickly extract areas of interest:

```
cutout reference.png aoi.json im1.png im2.jpg im3.png
```

Each image passed in will produce a folder of the extracted areas of interest
