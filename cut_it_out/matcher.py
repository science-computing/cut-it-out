from __future__ import print_function
import cv2
import numpy as np
from .aoi_collection import AOICollection


def load_image(filename):
    """Quick alias to read an image"""
    return cv2.imread(filename, cv2.IMREAD_COLOR)


def save_image(filename, image):
    """Quick alias to write an image that looks like above"""
    cv2.imwrite(filename, image)


class ImageMatcher:
    """A construct which, given a reference image, will attempt to align other
    images to said reference images, then extract specified points of interest

    Args:
        source_img (filename/handler):
        aoi_data (dict): A dictionary

    Attributes:
        aoi_data: The description of Areas of Interest
        detector: The feature detection algorithm (ORB)
        keypoints: cv2 KeyPoints from the source_img
        descriptors: cv2 Descriptors from the source_img
        matcher: A cv2 DescriptorMatcher (?)
        match_percent: The default percent of matches to keep

    Usage:
        Create the matcher then for each image you'd like to match
        run match (to just align) or extract (to pull out Areas of
        interest)
    """
    def __init__(self,
                 source_img,
                 aoi_data,
                 max_features=5000,
                 match_percent=0.10):
        self.source = source_img
        im_gray = cv2.cvtColor(source_img, cv2.COLOR_BGR2GRAY)
        self.aoi_data = aoi_data

        # Init our detector (we're using ORB)
        # https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_feature2d/py_orb/py_orb.html
        self.detector = cv2.ORB_create(max_features)

        # Create a list of max_features keypoints
        self.keypoints, self.descriptors =\
            self.detector.detectAndCompute(im_gray, None)

        # Run bruteforce matcher. We're using ORB, so we should use
        # Bruteforce matching with crosscheck
        # https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_feature2d/py_matcher/py_matcher.html#brute-force-matching-with-orb-descriptors
        self.matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

        self.match_percent = match_percent

    def match(self, image, debug_file=None, match_percent_override=None):
        im_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # Get keypoints from image...
        m_keypoints, m_descriptors =\
            self.detector.detectAndCompute(im_gray, None)

        # Use the matcher to attempt matching the image with the reference
        matches = self.matcher.match(m_descriptors, self.descriptors, None)

        # Sorting algorithm for our matches
        matches.sort(key=lambda x: x.distance, reverse=False)

        # Compute number of matches to retain
        # (literally just matches*match_percent)
        good_matches = int(
            len(matches) * (
                match_percent_override if match_percent_override is not None
                else self.match_percent
            )
        )
        matches = matches[:good_matches]

        s_points = np.zeros((len(matches), 2), dtype=np.float32)
        m_points = np.zeros((len(matches), 2), dtype=np.float32)

        for i, match in enumerate(matches):
            # Query our image we're trying to match
            m_points[i, :] = m_keypoints[match.queryIdx].pt
            # Train against our reference image
            s_points[i, :] = self.keypoints[match.trainIdx].pt

        # Find homography using RANSAC
        h, mask = cv2.findHomography(m_points, s_points, cv2.RANSAC, 5.0)

        # Make this a filename to dump the match image
        # Useful for debugging
        if debug_file is not None:
            matches_mask = mask.ravel().tolist()
            params = {
                'matchColor': (0, 255, 0),
                'singlePointColor': None,
                'matchesMask': matches_mask,
                'flags': 2
            }
            im_matches = cv2.drawMatches(
                self.source,
                self.keypoints,
                image,
                m_keypoints,
                matches,
                None,
                **params
            )
            save_image(debug_file, im_matches)

        # Warp the image to fit the reference
        height, width, channels = self.source.shape
        processed = cv2.warpPerspective(image, h, (width, height))
        return processed

    def extract(self, image):
        proc = self.match(image)
        return AOICollection(proc, self.aoi_data, root=True)
