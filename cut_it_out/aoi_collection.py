import cv2
import os


class AOIGlyph:
    """Not currently implemented"""
    def __init__(self):
        pass


class AOIObject:
    """An area of interest object

    contains image data (cv2) which has been extracted from the image
    """
    def __init__(self, data, error=None):
        self.data = data
        self.error = error

    @property
    def white_level(self, thresh=127):
        """Outputs a percentage of white pixels"""
        if self.error:
            return None
        if not hasattr(self, '_threshold'):

            # Because data comes in as BGR, size is 3 times
            # too large...
            pixels = self.data.size / 3

            # Need to convert back to grayscale here
            # There's definitely some optimization potential here
            t = cv2.threshold(
                cv2.cvtColor(self.data, cv2.COLOR_BGR2GRAY),
                0,
                255,
                cv2.THRESH_OTSU
            )[1]

            self._threshold = 1.0 * cv2.countNonZero(t) / pixels
        return self._threshold


class AOICollection:
    """A defined collection of Aread of Interest

    Args:
        image (cv2 image): the image which we are extracting from, already
            mapped to the reference image
        aoidef (json string): the JSON defining which areas of interest to
            extract
        root (bool): Whether this is the root collection, or a sub-collection
    """
    def __init__(self, image, aoidef, root=False):
        # TODO validate aoidev
        if root:
            self.image = image
        self._registered = []
        for label, val in aoidef.items():
            if val['type'] in ['rect', 'circle']:
                data = None
                error = None
                try:
                    x = val['x']
                    y = val['y']
                    w = val['w']
                    h = val['h']
                    data = image[y:y+h, x:x+w]
                except:
                    error = "Could not extract data"
                self._registered.append(label)
                setattr(self, str(label), AOIObject(data, error))
            elif val['type'] == 'glyph':
                pass
            elif val['type'] == 'group':
                self._registered.append(label)
                setattr(self, label, AOICollection(image, val['items']))

    def dump_to_folder(self, path, prefix=''):
        """Creates a folder and dumps all of the extracted images into it"""
        if not os.path.exists(path):
            os.makedirs(path)
        for x in self._registered:
            aoi = getattr(self, x)
            if isinstance(aoi, AOICollection):
                aoi.dump_to_folder(path, prefix='{}{}__'.format(prefix, x))
            elif isinstance(aoi, AOIObject):
                if aoi.data is not None:
                    cv2.imwrite(
                        os.path.join(
                            path, '{}{}.{}'.format(prefix, x, 'jpg')
                        ),
                        aoi.data
                    )

